const express = require('express');
const SendNotifition = require('./src/Notification')
const server = express();

server.get('/prpo/approver_isyou/:id/:number', SendNotifition.Send);

server.listen('3100', cb => {
    console.log('Server is starting');
});